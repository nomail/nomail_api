![NoMail](https://i.imgur.com/urLtCpp.png)

# NoMail
NoMail est un service de courriels encryptés.

### Wiki
Consultez notre [wiki](https://gitlab.com/nomail/nomail_api/-/wikis/API-NoMail) pour des informations et spécifications techniques.

#### Logo
Fait par [krvtos](mailto:krvtostv@gmail.com)
