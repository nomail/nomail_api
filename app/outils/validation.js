const Joi = require("joi");


// Soumission d'un courriel
const validerCourriel = données => {
	const schémaValidationCourriel = Joi.object({
			objet: Joi.string().min(0).max(64).default(null),
			contenu: Joi.string().min(10).max(51200).required(),
			adresseExpéditeur: Joi.string().min(8).max(255).required(),
			adresseDestinataire: Joi.string().min(8).max(255).required(),
		})
		.with("contenu", "adresseExpéditeur")
		.with("adresseExpéditeur", "adresseDestinataire");
	const validation = schémaValidationCourriel.validate(données);
	return validation;
};

// Soumission d'un utilisateur
const validerUtilisateurInscription = données => {
	const schémaValidationUtilisateur = Joi.object({
			adresseCourriel: Joi.string().min(8).max(255).email().required(),
			motDePasse: Joi.string().min(8).max(2048).required(),
			phraseDePasse: Joi.string().min(8).max(4096).required(),
			typeUtilisateur: Joi.string().max(15),
			cléPublique: Joi.string(),
			cléPrivée: Joi.string(),
		})
		.with("adresseCourriel", "motDePasse")
		.with("motDePasse", "phraseDePasse");
	const validation = schémaValidationUtilisateur.validate(données);
	return validation;
};

// Connexion d'un utilisateur
const validerUtilisateurConnexion = données => {
	const schémaValidationUtilisateur = Joi.object({
		adresseCourriel: Joi.string().min(8).max(255).email().required(),
		motDePasse: Joi.string().min(8).max(1024).required(),
	}).with("adresseCourriel", "motDePasse");
	const validation = schémaValidationUtilisateur.validate(données);
	return validation;
};

module.exports.validerUtilisateurInscription = validerUtilisateurInscription;
module.exports.validerUtilisateurConnexion = validerUtilisateurConnexion;
module.exports.validerCourriel = validerCourriel;
