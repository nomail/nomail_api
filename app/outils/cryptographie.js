const crypto = require("crypto");


// Encoder avec clé publique
const encoderDonnéesAvecCléPubliqueRSA = ((donnéesDécodées, cléPublique) => {
	if (!donnéesDécodées || !cléPublique) {
		return null;
	}

	const bufferDonnées = Buffer.from(donnéesDécodées, "utf8");
	const donnéesEncodées = crypto.publicEncrypt(cléPublique, bufferDonnées);
	return donnéesEncodées.toString("base64");
});

// Décoder avec clé privée
const décoderDonnéesAvecCléPrivéeRSA = ((donnéesEncodées, phraseDePasse, cléPrivée) => {
	if (!donnéesEncodées || !phraseDePasse || !cléPrivée) {
		return null;
	}

	var donnéesDécodées = "";

	const bufferDonnées = Buffer.from(donnéesEncodées, "base64");
	try {
		donnéesDécodées = crypto.privateDecrypt({
			key: cléPrivée.toString(),
			passphrase: phraseDePasse,
		}, bufferDonnées);
	} catch {
		return "Erreur de décodage"
	}

	return donnéesDécodées.toString("utf8");
});

// Encoder avec secret
const hasherDonnéesAvecSecret = ((donnéesDécodées, algorithme, secret) => {
	if (!donnéesDécodées || !algorithme || !secret) {
		return null;
	}

	const hmac = crypto.createHmac(algorithme, secret);
	hmac.update(donnéesDécodées);
	return hmac.digest('hex');
});

module.exports.encoderDonnéesAvecCléPubliqueRSA = encoderDonnéesAvecCléPubliqueRSA;
module.exports.décoderDonnéesAvecCléPrivéeRSA = décoderDonnéesAvecCléPrivéeRSA;
module.exports.hasherDonnéesAvecSecret = hasherDonnéesAvecSecret;
