const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require('dotenv/config');
const logger = require('morgan');
const fs = require('fs');
const app = express();

const date = new Date();
const jour = ("0" + date.getDate()).slice(-2);
const mois = ("0" + (date.getMonth() + 1)).slice(-2);
const année = date.getFullYear();

const préfixeURI = process.env.PREFIXE_URI;

// Importation des routes
const routeRacine = require("./routes/racine/racine");
const routeValidationToken = require("./routes/token/validerToken");
const quatreCentQuatre = require("./routes/racine/404");
const routeConnexion = require("./routes/authentification/connexion");
const routeInscription = require("./routes/authentification/inscription");
const routeUtilisateurs = require("./routes/utilisateur/utilisateurs");
const routeCourriels = require("./routes/courriel/courriels");
const routeCourrielsReçus = require("./routes/courriel/courrielsReçus");
const routeCourrielsEnvoyés = require("./routes/courriel/courrielsEnvoyés");


// Middlewares
app.use(logger('combined', {
	stream: fs.createWriteStream(`./journaux/${jour}-${mois}-${année}.log`, {
		flags: 'a',
	})
}));
app.use(express.urlencoded({
	extended: true,
}));
app.use(cors());
app.use(express.json());

// Attributions de routes
app.use(préfixeURI + "/", routeRacine);
app.use(préfixeURI + "/token", routeValidationToken);
app.use(préfixeURI + "/inscription", routeInscription);
app.use(préfixeURI + "/connexion", routeConnexion);
app.use(préfixeURI + "/courriels", routeCourriels);
app.use(préfixeURI + "/courriels/recu", routeCourrielsReçus);
app.use(préfixeURI + "/courriels/envoye", routeCourrielsEnvoyés);
app.use(préfixeURI + "/utilisateurs", routeUtilisateurs);
app.use("/", quatreCentQuatre);

// BD
mongoose.connect(
	process.env.CONNEXION_BD, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true,
		useFindAndModify: false,
	},
	() => console.log("Connecté à la BD"),
);

// SERVEUR
app.listen(3000);
