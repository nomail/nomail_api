const dotenv = require('dotenv');
const dotenvVars = require('dotenv-parse-variables');
let config = dotenv.config({})
if (config.error) throw config.error;
config = dotenvVars(config.parsed);

const router = require("express").Router();
const crypto = require("crypto");
const Utilisateur = require("../../modèles/utilisateur/Utilisateur");
const vérifierToken = require("../../middlewares/vérifierToken");
const {
	vérifierPermissionsAdmin,
} = require("../../middlewares/vérifierPermissions");
const {
	nombreAléatoire,
} = require("../../outils/divers");
const {
	isValidObjectId
} = require("mongoose");


// GET
// Tous les utilsateurs
router.get("/", [vérifierToken, vérifierPermissionsAdmin], async (req, res) => {
	await Utilisateur.find({}, async (erreur, utilisateurs) => {
		if (!erreur && typeof utilisateurs[0] != "undefined") {
			res.status(200)
				.send(utilisateurs);
		} else {
			res.status(404)
				.send({
					message: "Ressource non trouvée",
				});
		}
	}).select({
		"_id": 1,
		"adresseCourriel": 1,
		"cléPublique": 1,
	})
});

// UPDATE
// Mettre à jour le mot de passe d'un utilisateur (adresseCourriel, ancienMotDePasse, nouveauMotDePasse)
router.patch("/", vérifierToken, async (req, res) => {
	const adresseCourriel = req.body.adresseCourriel;
	const ancienMotDePasse = req.body.ancienMotDePasse;
	const nouveauMotDePasse = req.body.nouveauMotDePasse;
	const selNouveauMotDePasse = crypto.randomBytes(128).toString('base64');
	const itérationsNouveauMotDePasse = nombreAléatoire(config.NOMBRE_MIN_ITERATIONS, config.NOMBRE_MAX_ITERATIONS);
	await Utilisateur.findOne({
		adresseCourriel: adresseCourriel,
	}, async (erreur, ancienUtilisateurBD) => {
		if (erreur && !ancienUtilisateurBD) {
			res.status(404)
				.send({
					message: "Ressource non trouvée",
				});
			return;
		}
		const selAncienMotDePasse = ancienUtilisateurBD.motDePasse.sel;
		const itérationsAncienMotDePasse = ancienUtilisateurBD.motDePasse.itérations;

		crypto.pbkdf2(ancienMotDePasse, selAncienMotDePasse, itérationsAncienMotDePasse, 64, "sha512",
			async (erreur, cléDérivéeAncienMotDePasse) => {

				if (erreur) {
					res.status(500)
						.send({
							message: "Erreur interne",
						});
					return;
				}

				const hashAncienMotDePasse = cléDérivéeAncienMotDePasse.toString('hex');

				if (hashAncienMotDePasse == ancienUtilisateurBD.motDePasse.hash) {
					crypto.pbkdf2(nouveauMotDePasse, selNouveauMotDePasse, itérationsNouveauMotDePasse, 64, "sha512",
						async (erreur, cléDérivéeNouveauMotDePasse) => {
							const hashNouveauMotDePasse = cléDérivéeNouveauMotDePasse.toString('hex');
							if (erreur) {
								res.status(404)
									.send({
										message: "Ressource non trouvée",
									});
								return;
							}
							await Utilisateur.findOneAndUpdate({
								adresseCourriel: req.body.adresseCourriel,
							}, {
								$set: {
									motDePasse: {
										hash: hashNouveauMotDePasse,
										sel: selNouveauMotDePasse,
										itérations: itérationsNouveauMotDePasse,
									},
								}
							}, {
								new: true
							}, (erreur, utilisateurMisÀJour) => {
								if (!erreur && utilisateurMisÀJour != null) {
									res.status(200)
										.send({
											_id: utilisateurMisÀJour._id,
											adresseCourriel: utilisateurMisÀJour.adresseCourriel,
											cléPublique: utilisateurMisÀJour.cléPublique,
										});
								} else {
									res.status(404)
										.send({
											message: "Ressource non trouvée",
										});
								}
							});
						});
				} else {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
				}
			});
	});
});

// Obtenir un utilisateur spécifique (adresseCourriel)
router.get("/:filtre", vérifierToken, async (req, res) => {
	if (!req.params.filtre) {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
		return;
	}

	if (isValidObjectId(req.params.filtre)) {
		await Utilisateur.findById(req.params.filtre, async (erreur, utilisateurBD) => {
			if (!erreur && utilisateurBD) {
				res.status(200)
					.send({
						_id: utilisateurBD._id,
						adresseCourriel: utilisateurBD.adresseCourriel,
						cléPublique: utilisateurBD.cléPublique,
					});
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		});
	} else {
		await Utilisateur.findOne({
			adresseCourriel: req.utilisateur.adresseCourriel,
		}, (erreur, utilisateur) => {
			if (!erreur && utilisateur != null) {
				res.status(200)
					.send({
						_id: utilisateur._id,
						adresseCourriel: utilisateur.adresseCourriel,
						cléPublique: utilisateur.cléPublique,
					});
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		});
	}
});

// POST
// Soumettre un utilisateur
// Dans ../routes/auth

// DELETE
// Supprimer un utilisateur spécifique (adresseCourriel)
router.delete("/:filtre", vérifierToken, async (req, res) => {
	if (!req.params.filtre) {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
		return;
	}
	if (isValidObjectId(req.params.filtre)) {
		try {
			const utilisateurSupprimé = await Utilisateur.findByIdAndRemove(req.params.filtre);
			res.status(200)
				.send(utilisateurSupprimé);
		} catch (erreur) {
			res.status(500)
				.send({
					message: "Erreur interne",
				});
		}
	} else {
		try {
			const utilisateurSupprimé = await Utilisateur.remove({
				adresseCourriel: req.params.filtre,
			});
			res.status(200)
				.send(utilisateurSupprimé);
		} catch (erreur) {
			res.status(500)
				.send({
					message: "Erreur interne",
				});
		}
	}
});


module.exports = router;
