const router = require("express").Router();


// GET
// Tous les utilsateurs
router.get("/", async (req, res) => {
	res.status(200)
		.send({
			message: "Bienvenue sur l'API de NoMail",
		});
});

module.exports = router;
