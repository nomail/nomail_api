const router = require("express").Router();


// ALL
router.all("/*", async (req, res) => {
	res.status(404)
		.send({
			message: "Ressource non trouvée",
		});
});

module.exports = router;
