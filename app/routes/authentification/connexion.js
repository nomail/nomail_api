const dotenv = require('dotenv');
const dotenvVars = require('dotenv-parse-variables');
let config = dotenv.config({})
if (config.error) throw config.error;
config = dotenvVars(config.parsed);

const router = require("express").Router();
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const {
	hasherDonnéesAvecSecret,
} = require("../../outils/cryptographie");
const {
	validerUtilisateurConnexion,
} = require("../../outils/validation");
const Utilisateur = require("../../modèles/utilisateur/Utilisateur");


// Connecter un utilisateur
router.post("/", async (req, res) => {
	// Validation
	const {
		erreur,
	} = validerUtilisateurConnexion(req.body);

	if (!req.body.adresseCourriel || !req.body.motDePasse || !req.body.phraseDePasse) {
		res.status(401)
			.send({
				message: "Utilisateur non autorisé",
			});
		return;
	}

	if (erreur != undefined) {
		res.status(401)
			.send({
				message: "Utilisateur non autorisé",
			});
		return;
	} else {
		const adresseCourrielEncodée = Buffer.from(req.body.adresseCourriel).toString('base64').replace(/=/g, "");
		await Utilisateur.findOne({
			adresseCourriel: adresseCourrielEncodée,
		}, (erreur, utilisateurBD) => {
			if (!erreur && utilisateurBD != null) {
				// Hash mot de passe
				const sel = utilisateurBD.motDePasse.sel;
				const itérations = utilisateurBD.motDePasse.itérations;

				crypto.pbkdf2(req.body.motDePasse, sel, itérations, 64, "sha512",
					(erreur, cléDérivée) => {
						if (!erreur) {
							const hashMotDePasseUtilisateur = cléDérivée.toString('hex');
							const phraseDePasseHashée = hasherDonnéesAvecSecret(config.PREUVE_HMAC, config.ALGORITHME_HMAC, req.body.phraseDePasse);
							if (utilisateurBD.motDePasse.hash == hashMotDePasseUtilisateur &&
								utilisateurBD.phraseDePasse == phraseDePasseHashée.substring(config.NOMBRE_MIN_PHRASE_DE_PASSE, config.NOMBRE_MAX_PHRASE_DE_PASSE)) {

								// Créer et assigner un token
								const token = jwt.sign({
									_id: utilisateurBD._id,
									typeUtilisateur: utilisateurBD.typeUtilisateur,
									adresseCourriel: utilisateurBD.adresseCourriel,
									motDePasse: utilisateurBD.motDePasse.hash,
									phraseDePasse: phraseDePasseHashée,
								}, config.TOKEN_SECRET, {
									expiresIn: config.EXPIRATION_TOKEN,
									algorithm: "HS384",
								});
								res.status(200)
									.send({
										token: token,
									});
							} else {
								res.status(401)
									.send({
										message: "Utilisateur non autorisé",
									});
							}
						} else {
							res.status(500)
								.send({
									message: "Erreur interne",
								});
						}
					});
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		});
	}
});

module.exports = router;
