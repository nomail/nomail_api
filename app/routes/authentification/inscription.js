const dotenv = require('dotenv');
const dotenvVars = require('dotenv-parse-variables');
let config = dotenv.config({})
if (config.error) throw config.error;
config = dotenvVars(config.parsed);


const router = require("express").Router();
const crypto = require("crypto");
const {
	hasherDonnéesAvecSecret,
} = require("../../outils/cryptographie");
const {
	nombreAléatoire,
} = require("../../outils/divers");
const {
	validerUtilisateurInscription,
} = require("../../outils/validation");
const Utilisateur = require("../../modèles/utilisateur/Utilisateur");


// POST
// Soumettre un utilisateur
router.post("/", async (req, res) => {
	// Validation
	const validation = validerUtilisateurInscription(req.body);

	if (!req.body.adresseCourriel || !req.body.motDePasse || !req.body.phraseDePasse) {
		res.status(400)
			.send({
				message: "Requête invalide",
			});
		return;
	}

	if (validation.error != null) {
		res.status(422)
			.send({
				message: validation.error.details[0].message,
			});
		return;
	}

	// Hash mot de passe
	const sel = crypto.randomBytes(128).toString('base64');
	const itérations = nombreAléatoire(config.NOMBRE_MIN_ITERATIONS, config.NOMBRE_MAX_ITERATIONS);
	crypto.pbkdf2(req.body.motDePasse, sel, itérations, 64, "sha512",
		(erreur, cléDérivée) => {
			const hashMotDePasse = cléDérivée.toString('hex');
			if (!erreur) {
				// Encodage de l'adresse courriel (Base64) et mot de passe (hash) et sauvegarde de l'utilisateur
				const adresseCourrielEncodée = Buffer.from(req.body.adresseCourriel).toString('base64').replace(/=/g, "");
				crypto.generateKeyPair('rsa', {
					modulusLength: 4096,
					publicKeyEncoding: {
						type: 'spki',
						format: 'pem',
					},
					privateKeyEncoding: {
						type: 'pkcs8',
						format: 'pem',
						cipher: 'aes-256-cbc',
						passphrase: hasherDonnéesAvecSecret(config.PREUVE_HMAC, config.ALGORITHME_HMAC, req.body.phraseDePasse),
					}
				}, async (erreur, publicKey, privateKey) => {
					if (!erreur) {
						// Création d'un nouvel utilisateur
						const utilisateur = new Utilisateur({
							adresseCourriel: adresseCourrielEncodée,
							motDePasse: {
								hash: hashMotDePasse,
								sel: sel,
								itérations: itérations,
							},
							phraseDePasse: (hasherDonnéesAvecSecret(config.PREUVE_HMAC, config.ALGORITHME_HMAC, req.body.phraseDePasse) + '')
								.substring(config.NOMBRE_MIN_PHRASE_DE_PASSE, config.NOMBRE_MAX_PHRASE_DE_PASSE),
							typeUtilisateur: req.body.typeUtilisateur,
							cléPublique: publicKey,
							cléPrivée: privateKey,
						});
						try {
							const utilisateurSauvegardé = await utilisateur.save();
							res.status(200)
								.send({
									adresseCourriel: utilisateurSauvegardé.adresseCourriel,
									typeUtilisateur: utilisateurSauvegardé.typeUtilisateur,
									cléPublique: utilisateurSauvegardé.cléPublique,
								});
						} catch (erreur) {
							res.status(400)
								.send({
									message: "Requête invalide",
								});
						}
					} else {
						res.status(500)
							.send({
								message: "Erreur interne",
							});
					}
				});
			} else {
				res.status(500)
					.send({
						message: "Erreur interne",
					});
			}
		});
});

module.exports = router;
