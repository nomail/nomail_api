const router = require("express").Router();
const {
	décoderDonnéesAvecCléPrivéeRSA,
} = require("../../outils/cryptographie");
const vérifierToken = require("../../middlewares/vérifierToken");
const CourrielEnvoyé = require("../../modèles/courriel/CourrielEnvoyé");
const Utilisateur = require("../../modèles/utilisateur/Utilisateur");
const {
	isValidObjectId
} = require("mongoose");


// GET
// Obtenir un/des courriel(s) envoyé(s) (id/adresseCourriel)
router.get("/:filtre", vérifierToken, async (req, res) => {
	if (!req.params.filtre) {
		res.status(400)
			.send({
				message: "Mauvaise requête",
			});
		return;
	}

	if (isValidObjectId(req.params.filtre)) {
		await CourrielEnvoyé.findById(req.params.filtre, async (erreur, courrielEnvoyé) => {
			if (!erreur && courrielEnvoyé) {
				if (req.utilisateur.adresseCourriel !== courrielEnvoyé.courriel.adresseExpéditeur) {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}

				await Utilisateur.findOne({
					adresseCourriel: req.utilisateur.adresseCourriel,
				}, async (erreur, utilisateurBD) => {
					if (!erreur && utilisateurBD) {
						res.status(200)
							.send({
								_id: courrielEnvoyé._id,
								idCourrielReçu: courrielEnvoyé.idCourrielReçu,
								lu: courrielEnvoyé.lu,
								courriel: {
									objet: courrielEnvoyé.courriel.objet,
									contenu: décoderDonnéesAvecCléPrivéeRSA(courrielEnvoyé.courriel.contenu, req.utilisateur.phraseDePasse, utilisateurBD.cléPrivée),
									adresseExpéditeur: courrielEnvoyé.courriel.adresseExpéditeur,
									adresseDestinataire: courrielEnvoyé.courriel.adresseDestinataire,
								},
								dateCréation: courrielEnvoyé.dateCréation,
							});
						return;
					}
				});
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		});
	} else {
		await CourrielEnvoyé.find({
			"courriel.adresseExpéditeur": req.params.filtre,
		}, (erreur, courrielsEnvoyés) => {
			if ((!erreur && courrielsEnvoyés) && courrielsEnvoyés.length > 0) {
				if (req.utilisateur.adresseCourriel !== courrielsEnvoyés[0].courriel.adresseExpéditeur) {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}

				res.status(200)
					.send(courrielsEnvoyés);
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		}).select({
			"_id": 1,
			"lu": 1,
			"archivé": 1,
			"dateCréation": 1,
			"courriel": {
				"objet": 1,
				"adresseExpéditeur": 1,
				"adresseDestinataire": 1,
			},
		}).sort({
			dateCréation: -1,
		});
	}
});

// PATCH
// Mettre à jour le champs lu d'un courriel (id)
router.patch("/lu/:id", vérifierToken, async (req, res) => {
	if (req.params.id && isValidObjectId(req.params.id)) {

		CourrielEnvoyé.findById(req.params.id, (erreur, courrielEnvoyéBD) => {
			if (!erreur && courrielEnvoyéBD) {
				if (req.utilisateur.adresseCourriel == courrielEnvoyéBD.courriel.adresseExpéditeur) {
					CourrielEnvoyé.findByIdAndUpdate(req.params.id, {
						lu: courrielEnvoyéBD.lu == false ? true : false,
					}, {
						new: true,
					}, (erreur, courrielEnvoyéModifié) => {

						if (!erreur && courrielEnvoyéModifié) {
							res.status(269)
								.send({
									_id: courrielEnvoyéModifié._id,
									idCourrielReçu: courrielEnvoyéModifié.idCourrielReçu,
									lu: courrielEnvoyéModifié.lu,
									courriel: {
										objet: courrielEnvoyéModifié.courriel.objet,
										adresseExpéditeur: courrielEnvoyéModifié.courriel.adresseExpéditeur,
										adresseDestinataire: courrielEnvoyéModifié.courriel.adresseDestinataire,
									},
									dateCréation: courrielEnvoyéModifié.dateCréation,
								});
						} else {
							res.status(404)
								.send({
									message: "Ressource non trouvée",
								});
							return;
						}
					});
				} else {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
				return;
			}
		});
	} else {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
	}
});

// DELETE
// Supprimer un courriel envoyé (id)
router.delete("/:id", vérifierToken, async (req, res) => {
	if (!isValidObjectId(req.params.id)) {
		res.status(400)
			.send({
				message: "Mauvaise requête",
			});
		return;
	}

	await CourrielEnvoyé.findByIdAndRemove(req.params.id, (erreur, courrielEnvoyéSupprimé) => {
		if (!erreur && courrielEnvoyéSupprimé) {
			res.status(200)
				.send({
					_id: courrielEnvoyéSupprimé._id,
					idCourrielReçu: courrielEnvoyéSupprimé.idCourrielReçu,
					lu: courrielEnvoyéSupprimé.lu,
					courriel: {
						objet: courrielEnvoyéSupprimé.courriel.objet,
						adresseExpéditeur: courrielEnvoyéSupprimé.courriel.adresseExpéditeur,
						adresseDestinataire: courrielEnvoyéSupprimé.courriel.adresseDestinataire,
					},
					dateCréation: courrielEnvoyéSupprimé.dateCréation,
					dateSuppression: courrielEnvoyéSupprimé.dateModification,
				});
		} else {
			res.status(404)
				.send({
					message: "Ressource non trouvée",
				});
		}
	});
});

module.exports = router;
