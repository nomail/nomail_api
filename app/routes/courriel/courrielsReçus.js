const router = require("express").Router();
const {
	décoderDonnéesAvecCléPrivéeRSA,
} = require("../../outils/cryptographie");
const vérifierToken = require("../../middlewares/vérifierToken");
const CourrielReçu = require("../../modèles/courriel/CourrielReçu");
const Utilisateur = require("../../modèles/utilisateur/Utilisateur");
const {
	isValidObjectId
} = require("mongoose");


// GET
// Obtenir un/des courriel(s) reçu(s) (id/adresseCourriel)
router.get("/:filtre", vérifierToken, async (req, res) => {
	if (!req.params.filtre) {
		res.status(400)
			.send({
				message: "Mauvaise requête",
			});
		return;
	}

	if (isValidObjectId(req.params.filtre)) {
		await CourrielReçu.findOne({
			_id: req.params.filtre,
			archivé: false,
		}, async (erreur, courrielReçu) => {
			if (!erreur && courrielReçu) {
				if (req.utilisateur.adresseCourriel !== courrielReçu.courriel.adresseDestinataire) {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}

				await Utilisateur.findById(req.utilisateur._id, async (erreur, utilisateurBD) => {
					if (!erreur && utilisateurBD) {
						res.status(200)
							.send({
								_id: courrielReçu._id,
								lu: courrielReçu.lu,
								archivé: courrielReçu.archivé,
								courriel: {
									objet: courrielReçu.courriel.objet,
									contenu: décoderDonnéesAvecCléPrivéeRSA(courrielReçu.courriel.contenu, req.utilisateur.phraseDePasse, utilisateurBD.cléPrivée),
									adresseExpéditeur: courrielReçu.courriel.adresseExpéditeur,
									adresseDestinataire: courrielReçu.courriel.adresseDestinataire,
								},
								dateCréation: courrielReçu.dateCréation,
							});
					}
				});
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée"
					});
				return;
			}
		});
	} else if (req.params.filtre) {
		await CourrielReçu.find({
			"courriel.adresseDestinataire": req.params.filtre,
			archivé: false,
		}, (erreur, courrielsReçus) => {
			if ((!erreur && courrielsReçus) && courrielsReçus.length > 0) {
				if (req.utilisateur.adresseCourriel !== courrielsReçus[0].courriel.adresseDestinataire) {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}

				res.status(200)
					.send(courrielsReçus);
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		}).select({
			"_id": 1,
			"lu": 1,
			"archivé": 1,
			"dateCréation": 1,
			"courriel": {
				"objet": 1,
				"adresseExpéditeur": 1,
				"adresseDestinataire": 1,
			},
		}).sort({
			dateCréation: -1,
		});
	} else {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
	}
});

// Obtenir un/des courriel(s) reçu(s) archivé(s) (id/adresseCourriel)
router.get("/archive/:filtre", vérifierToken, async (req, res) => {
	if (!req.params.filtre) {
		res.status(400)
			.send({
				message: "Mauvaise requête",
			});
		return;
	}

	if (isValidObjectId(req.params.filtre)) {
		await CourrielReçu.findOne({
			_id: req.params.filtre,
			archivé: true,
		}, async (erreur, courrielReçu) => {
			if (!erreur && courrielReçu) {
				if (req.utilisateur.adresseCourriel !== courrielReçu.courriel.adresseDestinataire) {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}

				await Utilisateur.findById(req.utilisateur._id, async (erreur, utilisateurBD) => {
					if (!erreur && utilisateurBD) {
						res.status(200)
							.send({
								_id: courrielReçu._id,
								lu: courrielReçu.lu,
								archivé: courrielReçu.archivé,
								courriel: {
									objet: courrielReçu.courriel.objet,
									contenu: décoderDonnéesAvecCléPrivéeRSA(courrielReçu.courriel.contenu, req.utilisateur.phraseDePasse, utilisateurBD.cléPrivée),
									adresseExpéditeur: courrielReçu.courriel.adresseExpéditeur,
									adresseDestinataire: courrielReçu.courriel.adresseDestinataire,
								},
								dateCréation: courrielReçu.dateCréation,
							});
					}
				});
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée"
					});
				return;
			}
		});
	} else if (req.params.filtre) {
		await CourrielReçu.find({
			"courriel.adresseDestinataire": req.params.filtre,
			archivé: true,
		}, (erreur, courrielsReçus) => {
			if ((!erreur && courrielsReçus) && courrielsReçus.length > 0) {
				if (req.utilisateur.adresseCourriel !== courrielsReçus[0].courriel.adresseDestinataire) {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}

				res.status(200)
					.send(courrielsReçus);
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
			}
		}).select({
			"_id": 1,
			"lu": 1,
			"archivé": 1,
			"dateCréation": 1,
			"courriel": {
				"objet": 1,
				"adresseExpéditeur": 1,
				"adresseDestinataire": 1,
			},
		}).sort({
			dateCréation: -1,
		});
	} else {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
	}
});

// PATCH
// Mettre à jour le champs lu d'un courriel (id)
router.patch("/lu/:id", vérifierToken, async (req, res) => {
	if (req.params.id && isValidObjectId(req.params.id)) {

		CourrielReçu.findById(req.params.id, (erreur, courrielReçuBD) => {
			if (!erreur && courrielReçuBD) {
				if (req.utilisateur.adresseCourriel == courrielReçuBD.courriel.adresseDestinataire) {
					CourrielReçu.findByIdAndUpdate(req.params.id, {
						lu: courrielReçuBD.lu == false ? true : false,
					}, {
						new: true,
					}, (erreur, courrielReçuModifié) => {

						if (!erreur && courrielReçuModifié) {
							res.status(269)
								.send({
									_id: courrielReçuModifié._id,
									idCourrielEnvoyé: courrielReçuModifié.idCourrielEnvoyé,
									lu: courrielReçuModifié.lu,
									archivé: courrielReçuModifié.archivé,
									courriel: {
										objet: courrielReçuModifié.courriel.objet,
										adresseExpéditeur: courrielReçuModifié.courriel.adresseExpéditeur,
										adresseDestinataire: courrielReçuModifié.courriel.adresseDestinataire,
									},
									dateCréation: courrielReçuModifié.dateCréation,
								});
						} else {
							res.status(404)
								.send({
									message: "Ressource non trouvée",
								});
							return;
						}
					});
				} else {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
				return;
			}
		});
	} else {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
	}
});

// Mettre à jour le champs archive d'un courriel recu (id)
router.patch("/archive/:id", vérifierToken, async (req, res) => {
	if (req.params.id && isValidObjectId(req.params.id)) {

		CourrielReçu.findById(req.params.id, (erreur, courrielReçuBD) => {
			if (!erreur && courrielReçuBD) {
				if (req.utilisateur.adresseCourriel == courrielReçuBD.courriel.adresseDestinataire) {
					CourrielReçu.findByIdAndUpdate(req.params.id, {
						archivé: courrielReçuBD.archivé == false ? true : false,
					}, {
						new: true,
					}, (erreur, courrielReçuModifié) => {

						if (!erreur && courrielReçuModifié) {
							res.status(269)
								.send({
									_id: courrielReçuModifié._id,
									lu: courrielReçuModifié.lu,
									archivé: courrielReçuModifié.archivé,
									courriel: {
										objet: courrielReçuModifié.courriel.objet,
										adresseExpéditeur: courrielReçuModifié.courriel.adresseExpéditeur,
										adresseDestinataire: courrielReçuModifié.courriel.adresseDestinataire,
									},
									dateCréation: courrielReçuModifié.dateCréation,
								});
						} else {
							res.status(404)
								.send({
									message: "Ressource non trouvée",
								});
							return;
						}
					});
				} else {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
					return;
				}
			} else {
				res.status(404)
					.send({
						message: "Ressource non trouvée",
					});
				return;
			}
		});
	} else {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
	}
});

// DELETE
// Supprimer un courriel reçu (id)
router.delete("/:id", vérifierToken, async (req, res) => {
	if (!isValidObjectId(req.params.id)) {
		res.status(404)
			.send({
				message: "Ressource non trouvée",
			});
		return;
	}

	await CourrielReçu.findByIdAndRemove(req.params.id, (erreur, courrielReçuSupprimé) => {
		if (!erreur && courrielReçuSupprimé) {
			res.status(200)
				.send({
					_id: courrielReçuSupprimé._id,
					lu: courrielReçuSupprimé.lu,
					archivé: courrielReçuSupprimé.archivé,
					courriel: {
						objet: courrielReçuSupprimé.courriel.objet,
						adresseExpéditeur: courrielReçuSupprimé.courriel.adresseExpéditeur,
						adresseDestinataire: courrielReçuSupprimé.courriel.adresseDestinataire,
					},
					dateCréation: courrielReçuSupprimé.dateCréation,
					dateSuppression: courrielReçuSupprimé.dateModification,
				});
		} else {
			res.status(404)
				.send({
					message: "Ressource non trouvée",
				});
		}
	});
});

module.exports = router;
