const router = require("express").Router();
const {
	validerCourriel
} = require("../../outils/validation");
const vérifierToken = require("../../middlewares/vérifierToken");
const CourrielEnvoyé = require("../../modèles/courriel/CourrielEnvoyé");
const Utilisateur = require("../../modèles/utilisateur/Utilisateur");



// POST
// Soumettre un courriel
router.post("/", vérifierToken, async (req, res) => {
	// Validation
	const {
		erreur,
	} = validerCourriel(req.body);

	if (req.utilisateur.adresseCourriel == req.body.adresseDestinataire) {
		res.status(400)
			.send({
				message: "Mauvaise requête",
			});
		return;
	}

	if (erreur != undefined) {
		res.status(422)
			.send({
				message: erreur.details,
			});
		return;
	}

	// Obtenir la clé publique du destinataire
	await Utilisateur.findOne({
		adresseCourriel: req.body.adresseDestinataire
	}, async (erreur, destinataire) => {

		if (!erreur && destinataire) {
			await Utilisateur.findOne({
				adresseCourriel: req.utilisateur.adresseCourriel,
			}, async (erreur, expéditeur) => {
				if (!erreur && expéditeur) {
					const courriel = {
						objet: req.body.objet,
						contenu: req.body.contenu,
						adresseExpéditeur: req.utilisateur.adresseCourriel,
						adresseDestinataire: req.body.adresseDestinataire,
					};

					const courrielEnvoyé = new CourrielEnvoyé({
						courriel: courriel,
					});

					courrielEnvoyé.save((erreur, courrielEnvoyéSauvegardé) => {
						if (!erreur && courrielEnvoyéSauvegardé) {
							res.status(200)
								.send({
									_id: courrielEnvoyéSauvegardé._id,
									idCourrielReçu: courrielEnvoyéSauvegardé.idCourrielReçu,
									lu: courrielEnvoyéSauvegardé.lu,
									courriel: {
										objet: req.body.objet,
										contenu: req.body.contenu,
										adresseExpéditeur: req.utilisateur.adresseCourriel,
										adresseDestinataire: req.body.adresseDestinataire,
									},
									dateCréation: courrielEnvoyéSauvegardé.dateCréation,
								});
						} else {
							res.status(422)
								.send({
									message: "Courriel non traitable",
								});
						}

					});
				} else {
					res.status(404)
						.send({
							message: "Ressource non trouvée"
						});
				}
			});
		} else {
			res.status(404)
				.send({
					message: "Ressource non trouvée"
				});
		}
	});
});

module.exports = router;
