const router = require("express").Router();
const vérifierToken = require("../../middlewares/vérifierToken");


// ALL
// Valider le token reçu
router.all("/", vérifierToken, async (req, res) => {
	res.status(200)
		.send({
			message: "Valide",
		});
});

module.exports = router;
