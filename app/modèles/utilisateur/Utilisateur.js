const mongoose = require("mongoose");

const SchemaUtilisateur = mongoose.Schema({
	adresseCourriel: {
		type: String,
		min: 8,
		max: 255,
		required: [true, "L'adresse courriel est requise"],
		unique: true,
		index: true,
	},
	motDePasse: {
		hash: {
			type: String,
			min: 10,
			max: 2048,
			required: [true, "Le hash du contenu est requis"],
		},
		sel: {
			type: String,
			min: 10,
			max: 2048,
			required: [true, "Le sel du contenu est requis"],
		},
		itérations: {
			type: Number,
			required: [true, "Le nombre d'itérations du contenu est requis"],
		}
	},
	phraseDePasse: {
		type: String,
		min: 10,
		max: 4096,
		required: [true, "La preuve de la phrase de passe est requise"],
	},
	typeUtilisateur: {
		type: String,
		max: 15,
		default: "normal",
		required: false,
	},
	cléPublique: {
		type: String,
		unique: true,
		required: [true, "La clé publique est requise"],
	},
	cléPrivée: {
		type: String,
		unique: true,
		required: [true, "La clé privée est requise"],
	},
}, {
	timestamps: {
		createdAt: "dateCréation",
		updatedAt: "dateModification",
	},
});


module.exports = mongoose.model("Utilisateur", SchemaUtilisateur);
