const SchémaCourriel = ({
	objet: {
		type: String,
		min: 0,
		max: 64,
		required: false,
		default: null,
	},
	contenu: {
		type: String,
		min: 10,
		max: 51200,
		required: [true, "Le contenu est requis"],
	},
	adresseExpéditeur: {
		type: String,
		min: 8,
		max: 255,
		required: [true, "L'adresse courriel de l'expéditeur est requise"],
	},
	adresseDestinataire: {
		type: String,
		min: 8,
		max: 255,
		required: [true, "L'adresse courriel du destinataire est requise"],
	},
});

module.exports = {
	SchémaCourriel,
};
