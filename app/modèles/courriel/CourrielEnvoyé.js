const mongoose = require("mongoose");
const Utilisateur = require("../utilisateur/Utilisateur");
const {
	encoderDonnéesAvecCléPubliqueRSA,
} = require("../../outils/cryptographie");
const {
	SchémaCourriel
} = require("./Courriel");


const SchémaCourrielEnvoyé = mongoose.Schema({
	idCourrielReçu: {
		type: String,
		required: false,
	},
	lu: {
		type: Boolean,
		default: false,
		required: false,
	},
	courriel: mongoose.Schema(SchémaCourriel),
}, {
	timestamps: {
		createdAt: "dateCréation",
		updatedAt: "dateModification",
	},
});

// Middleware
SchémaCourrielEnvoyé.pre("save", function (next) {
	const CourrielReçu = require("./CourrielReçu");

	if (this.courriel.adresseExpéditeur == this.courriel.adresseDestinataire) {
		throw new Error("Même utilisateur");
	}

	Utilisateur.find({
		$or: [{
				"adresseCourriel": this.courriel.adresseExpéditeur
			},
			{
				"adresseCourriel": this.courriel.adresseDestinataire
			}
		]
	}, (erreur, utilisateursBD) => {
		if (!erreur && utilisateursBD) {
			var courrielReçu = null;
			var cléDes;
			var cléExp;

			courrielReçu = new CourrielReçu({
				idCourrielEnvoyé: this._id,
				lu: false,
				courriel: this.courriel,
			});

			if (utilisateursBD[0].adresseCourriel == this.courriel.adresseExpéditeur) {
				cléExp = 0;
				cléDes = 1;
			} else {
				cléExp = 1;
				cléDes = 0;
			}

			courrielReçu.courriel.contenu = encoderDonnéesAvecCléPubliqueRSA(this.courriel.contenu, utilisateursBD[cléDes].cléPublique);

			courrielReçu.save((erreur, courrielReçuSauvegardé) => {
				if (!erreur) {
					this.idCourrielReçu = courrielReçuSauvegardé._id;
					this.courriel.contenu = encoderDonnéesAvecCléPubliqueRSA(this.courriel.contenu, utilisateursBD[cléExp].cléPublique);
					next();
				} else {
					throw new Error("Erreur BD");
				}
			});
		} else {
			throw new Error("Erreur BD");
		}
	});
});


module.exports = mongoose.model("CourrielEnvoyé", SchémaCourrielEnvoyé);
