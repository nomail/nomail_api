const mongoose = require("mongoose");
const {
	SchémaCourriel
} = require("./Courriel");


const SchémaCourrielReçu = mongoose.Schema({
	idCourrielEnvoyé: {
		type: String,
		required: false,
	},
	lu: {
		type: Boolean,
		default: false,
		required: false,
	},
	archivé: {
		type: Boolean,
		default: false,
		required: false,
	},
	courriel: {
		type: mongoose.Schema(SchémaCourriel),
		required: true,
	},
}, {
	timestamps: {
		createdAt: "dateCréation",
		updatedAt: "dateModification",
	},
});


module.exports = mongoose.model("CourrielReçu", SchémaCourrielReçu);
