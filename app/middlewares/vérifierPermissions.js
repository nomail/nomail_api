const dotenv = require('dotenv');
const dotenvVars = require('dotenv-parse-variables');
let config = dotenv.config({})
if (config.error) throw config.error;
config = dotenvVars(config.parsed);

var permissions = {
	"courriel": false,
	"utilisateur": false,
	"administrateur": false,
};

const vérifierPermissionsAdresseCourriel = async (req, res, next) => {
	switch (req.utilisateur.adresseCourriel) {
		case
		req.params.adresseCourriel,
		req.params.adresseDestinataire,
		req.params.adresseExpéditeur,
		req.params.filtre:
			permissions.courriel = true;
			next();
			break;
		case req.body.adresseCourriel,
		req.body.adresseDestinataire,
		req.body.adresseExpéditeur:
			permissions.courriel = true;
			next();
			break;
		default:
			res.status(401)
				.send({
					message: "Utilisateur non autorisé",
				});
			break;
	}
	req.permissions = permissions;
	return req.permissions;
}


const vérifierPermissionsIdUtilisateur = async (req, res, next) => {
	switch (req.utilisateur.id) {
		case
		req.params.id,
		req.params._id,
		req.params.filtre:
			permissions.utilisateur = true;
			next();
			break;
		case req.body.id,
		req.body._id:
			permissions.utilisateur = true;
			next();
			break;
		default:
			res.status(401)
				.send({
					message: "Utilisateur non autorisé",
				});
			break;
	}
	req.permissions = permissions;
}

const vérifierPermissionsAdmin = async (req, res, next) => {
	if (req.utilisateur.typeUtilisateur === "admin") {
		permissions.administrateur = true;
		next();
	} else {
		res.status(401)
			.send({
				message: "Utilisateur non autorisé",
			});
	}
	req.permissions = permissions;
}

module.exports.vérifierPermissionsAdresseCourriel = vérifierPermissionsAdresseCourriel;
module.exports.vérifierPermissionsIdUtilisateur = vérifierPermissionsIdUtilisateur;
module.exports.vérifierPermissionsAdmin = vérifierPermissionsAdmin;
