const dotenv = require('dotenv');
const dotenvVars = require('dotenv-parse-variables');
let config = dotenv.config({})
if (config.error) throw config.error;
config = dotenvVars(config.parsed);

const jwt = require("jsonwebtoken");
const {
	validerUtilisateurConnexion,
} = require("../outils/validation");
const Utilisateur = require("../modèles/utilisateur/Utilisateur");

module.exports = async function auth(req, res, next) {
	const token = req.header("Authorization");
	// Validation
	const {
		erreur,
	} = validerUtilisateurConnexion(req.body);

	if (!token && erreur != undefined) {
		res.status(401)
			.send({
				message: "Utilisateur non autorisé",
			});
		return;
	} else {
		try {
			const vérifié = jwt.verify(token, config.TOKEN_SECRET);
			req.utilisateur = vérifié;

			await Utilisateur.findOne({
				adresseCourriel: req.utilisateur.adresseCourriel,
			}, (erreur, utilisateurBD) => {
				if (!erreur && utilisateurBD) {
					if (req.utilisateur.phraseDePasse
						.substring(config.NOMBRE_MIN_PHRASE_DE_PASSE, config.NOMBRE_MAX_PHRASE_DE_PASSE) == utilisateurBD.phraseDePasse) {
						next();
					} else {
						res.status(401)
							.send({
								message: "Utilisateur non autorisé",
							});
					}

				} else {
					res.status(401)
						.send({
							message: "Utilisateur non autorisé",
						});
				}
			});
		} catch (erreur) {
			res.status(401)
				.send({
					message: "Utilisateur non autorisé",
				});
		}
	}
}
