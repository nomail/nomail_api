# Versions
Les versions de l’api sont accessibles à des URL distincts pour assurer la rétrocompatibilité.
| Version | URL      |
|---------|----------|
| 1       | /api/v1/ |

# Points d'entrée
## Publics
| Méthode | URL          | Description      | Paramètres                                                    | Réponse     |
|---------|--------------|------------------|---------------------------------------------------------------|-------------|
| POST    | /connexion   | Authentification | adresseCourriel, motDePasse, phraseDePasse                    | token JWT   |
| POST    | /inscription | Inscription      | adresseCourriel, motDePasse, phraseDePasse, [typeUtilisateur] | Utilisateur |

## Privés
| Méthode | URL                                  | Description                            | Paramètres                                             | Réponse          |
|---------|--------------------------------------|----------------------------------------|--------------------------------------------------------|------------------|
| GET     | /utilisateurs                        | Obtient une liste d'utilisateurs       |                                                        | [Utilisateur]    |
| GET     | /utilisateurs/{id}                   | Obtient un utilisateur                 |                                                        | Utilisateur      |
| GET     | /utilisateurs/{adresse courriel}     | Obtient un utilisateur                 |                                                        | Utilisateur      |
| GET     | /courriels/recu/{id}                 | Obtient un courriel reçu               |                                                        | CourrielReçu     |
| GET     | /courriels/recu/{adresse courriel}   | Obtient une liste de courriels reçus   |                                                        | [CourrielReçu]   |
| GET     | /courriels/envoye/{id}               | Obtient un courriel envoyé             |                                                        | CourrielEnvoyé   |
| GET     | /courriels/envoye/{adresse courriel} | Obtient une liste de courriels envoyés |                                                        | [CourrielEnvoyé] |
| POST    | /courriels                           | Soumet un courriel                     | objet, contenu, adresseExpéditeur, adresseDestinataire | CourrielEnvoyé   |
| PATCH   | /utilisateurs                        | Change le mot de passe                 | adresseCourriel, ancienMotDePasse, nouveauMotDePasse   | Utilisateur      |
| PATCH   | /courriels/lu                        | Bascule l'attribut lu                  |                                                        | CourrielEnvoyé   |
| PATCH   | /courriels/archive                   | Bascule l'attribut archivé             |                                                        | CourrielEnvoyé   |
| DELETE  | /utilisateurs/{id}                   | Supprime un utilisateur                |                                                        | Utilisateur      |
| DELETE  | /courriels/recu/{id}                 | Supprime un courriel reçu              |                                                        | CourrielReçu     |
| DELETE  | /courriels/envoye/{id}               | Supprime un courriel envoyé            |                                                        | CourrielEnvoyé   |

# Ressources
## Utilisateur
Un utilisateur du système.

### Identifiants:
* _id<br>
* adresseCourriel

### Propriétés:
| Nom             | Type            |
|-----------------|-----------------|
| _id             | ObjectId || str |
| adresseCourriel | str             |
| cléPublique     | str             |

## Courriel reçu
Un courriel reçu.

### Identifiant
* _id

### Propriétés
| Nom                 | Type            |
|---------------------|-----------------|
| _id                 | ObjectId || str |
| objet               | str             |
| archivé             | bool            |
| contenu             | str             |
| adresseExpéditeur   | str             |
| adresseDestinataire | str             |
| idCourrielEnvoyé    | ObjectId || str |
| dateCréation        | datetime        |
| dateModification    | datetime        |

## Courriel envoyé
Un courriel envoyé.

### Identifiant
* _id

### Propriétés
| Nom                 | Type            |
|---------------------|-----------------|
| _id                 | ObjectId || str |
| objet               | str             |
| lu                  | bool            |
| contenu             | str             |
| adresseExpéditeur   | str             |
| adresseDestinataire | str             |
| idCourrielReçu      | ObjectId || str |
| dateCréation        | datetime        |
| dateModification    | datetime        |

# Exemples
## Inscrire l'utilisateur `jdoe`
#### Entrée:
```
curl -X POST "http://nomail.com/api/v1/inscription" --data '{"adresseCourriel": "jdoe@email.com", "motDePasse": "motDePasse31!", "phraseDePasse": "ceci est une phrase de passe"}' -H "Content-Type: application/json"
```
#### Sortie:
```
{
    "adresseCourriel": "amRvZUBlbWFpbC5jb20",
    "typeUtilisateur": "normal",
    "cléPublique": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAzoc3UUKYrGw72oBR/ZgO\nFRq/qAnls12ejeEOCREQbFfui8vv37qhk5YKoOKR4qYTxTODSu10e8Q+oNEJ6q66\n//B8NHA1ti68E09cxrohL1rYj4K6X8ECFwzTp3Zn0w2ZxBthRPZJBJnDxviT2VCV\nmzWXVmgFzHprqqgggs05F02xB/6FTrBAQPPQOyyiw5TYbs+0Yv/pHG7hOVQpzYBL\nWnQfR24NCvMSRGkYyihHgseqsEdQFLCy2f40g1FmZVSp7lST9hT8sS/zJ3afR1tG\n/FoMUFd96BoVq7FupG1mSS8RpSZ7efBAITB5CdaqfrZLiY0TUdsuHk08XWxuGjrj\nBJZa931ZoY9xk84CNCCYBVZh3swod7iWY7dmatFHLP1Xe9uFfaBbQK63Ylv31cTL\n4PcDP5ZFESrguUzx+osT3EvvSpjoG4zR5GtDPWnSEp5DDvdt7/UOHoJ+3p58BNl1\nXwAP9/ljoNpnDGu7sz+ET7yIDSLM9ifN+zcdBRRQP3/DQu30A5U77fIVMnte+pW5\nGCRYuXom1XnQny2ph0RwrNisHGCTSKQukMfVQEqTsdTYOjB0r0gVmWq3Pw9OSvLC\nB9jMpOcisoFoHk3kP1VOW6xlbLIlBeHZIHaBQKOuQPL37CCHovX4in5Bo9Tw2GOP\nGliKXGXe65CXaEOPCfW3OyMCAwEAAQ==\n-----END PUBLIC KEY-----\n"
}
```

## Authentification en tant qu'utilisateur `jdoe`
#### Entrée:
```
curl "http://nomail.com/api/v1/connexion" --data '{"adresseCourriel": "jdoe@email.com", "motDePasse": "motDePasse31!", "phraseDePasse": "ceci est une phrase de passe"}' -H "Content-Type: application/json"
```
#### Sortie:
```
{
    "token": "eyJhbGciOiJIUzM4NCIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDdhNzc4NDY2M2NkMDZhMWNiYzExNDMiLCJ0eXBlVXRpbGlzYXRldXIiOiJub3JtYWwiLCJhZHJlc3NlQ291cnJpZWwiOiJhbVJ2WlVCbGJXRnBiQzVqYjIwIiwibW90RGVQYXNzZSI6IjczMjE5YWRkYjhhOTlhNGQxZjk4NjFkNzE0ZjI0ZjhjM2ZhOWMzYmYyMTNiZjkyNzViY2E5ZTc2MjJkODI3NzU1NTIxMmY4NDFiNWUzOWRkMjE1ZTg5NDJjZDA1NzVkNDMxN2UyYWIyYThjZTBmZjU1MTEwNjIwYWZiMjk1NzIzIiwicGhyYXNlRGVQYXNzZSI6IjEyZmQ2YTJlY2ZlMjk2NTk2YThhYmJlMTZmNjEzNjgzY2E0MmFiOWQxMTg1MzU0MjZiMGRiZmVlZWExMzQ1N2MiLCJpYXQiOjE2MTg2Mzg3MzgsImV4cCI6MTYxODcyNTEzOH0.BsPiJ79SSI6v9QULKvOHu9rs7nDtwWAEU1H9s7K166WLX0_EH6rH5rrvtJCs98VP"
}
```

## Obtenir le profil de l'utilisateur `jdoe` par son adresse courriel
#### Entrée:
```
curl "http://nomail.com/api/v1/utilisateurs/606fcc9e584e1a1af465d072" -H "Content-Type: application/json" -H "Authorization: <TOKEN>"
```
#### Sortie:
```
{
    "_id": "607a7784663cd06a1cbc1143",
    "adresseCourriel": "amRvZUBlbWFpbC5jb20",
    "cléPublique": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAzoc3UUKYrGw72oBR/ZgO\nFRq/qAnls12ejeEOCREQbFfui8vv37qhk5YKoOKR4qYTxTODSu10e8Q+oNEJ6q66\n//B8NHA1ti68E09cxrohL1rYj4K6X8ECFwzTp3Zn0w2ZxBthRPZJBJnDxviT2VCV\nmzWXVmgFzHprqqgggs05F02xB/6FTrBAQPPQOyyiw5TYbs+0Yv/pHG7hOVQpzYBL\nWnQfR24NCvMSRGkYyihHgseqsEdQFLCy2f40g1FmZVSp7lST9hT8sS/zJ3afR1tG\n/FoMUFd96BoVq7FupG1mSS8RpSZ7efBAITB5CdaqfrZLiY0TUdsuHk08XWxuGjrj\nBJZa931ZoY9xk84CNCCYBVZh3swod7iWY7dmatFHLP1Xe9uFfaBbQK63Ylv31cTL\n4PcDP5ZFESrguUzx+osT3EvvSpjoG4zR5GtDPWnSEp5DDvdt7/UOHoJ+3p58BNl1\nXwAP9/ljoNpnDGu7sz+ET7yIDSLM9ifN+zcdBRRQP3/DQu30A5U77fIVMnte+pW5\nGCRYuXom1XnQny2ph0RwrNisHGCTSKQukMfVQEqTsdTYOjB0r0gVmWq3Pw9OSvLC\nB9jMpOcisoFoHk3kP1VOW6xlbLIlBeHZIHaBQKOuQPL37CCHovX4in5Bo9Tw2GOP\nGliKXGXe65CXaEOPCfW3OyMCAwEAAQ==\n-----END PUBLIC KEY-----\n"
}
```

## Supprimer l'utilisateur `jdoe` par son id
#### Entrée:
```
curl -X DELETE "http://nomail.com/api/v1/utilisateurs/607a7784663cd06a1cbc1143" -H "Content-Type: application/json" -H "Authorization: <TOKEN>"
```
#### Sortie:
```
{
    "_id": "607a7784663cd06a1cbc1143",
    "adresseCourriel": "amRvZUBlbWFpbC5jb20",
    "cléPublique": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAzoc3UUKYrGw72oBR/ZgO\nFRq/qAnls12ejeEOCREQbFfui8vv37qhk5YKoOKR4qYTxTODSu10e8Q+oNEJ6q66\n//B8NHA1ti68E09cxrohL1rYj4K6X8ECFwzTp3Zn0w2ZxBthRPZJBJnDxviT2VCV\nmzWXVmgFzHprqqgggs05F02xB/6FTrBAQPPQOyyiw5TYbs+0Yv/pHG7hOVQpzYBL\nWnQfR24NCvMSRGkYyihHgseqsEdQFLCy2f40g1FmZVSp7lST9hT8sS/zJ3afR1tG\n/FoMUFd96BoVq7FupG1mSS8RpSZ7efBAITB5CdaqfrZLiY0TUdsuHk08XWxuGjrj\nBJZa931ZoY9xk84CNCCYBVZh3swod7iWY7dmatFHLP1Xe9uFfaBbQK63Ylv31cTL\n4PcDP5ZFESrguUzx+osT3EvvSpjoG4zR5GtDPWnSEp5DDvdt7/UOHoJ+3p58BNl1\nXwAP9/ljoNpnDGu7sz+ET7yIDSLM9ifN+zcdBRRQP3/DQu30A5U77fIVMnte+pW5\nGCRYuXom1XnQny2ph0RwrNisHGCTSKQukMfVQEqTsdTYOjB0r0gVmWq3Pw9OSvLC\nB9jMpOcisoFoHk3kP1VOW6xlbLIlBeHZIHaBQKOuQPL37CCHovX4in5Bo9Tw2GOP\nGliKXGXe65CXaEOPCfW3OyMCAwEAAQ==\n-----END PUBLIC KEY-----\n"
}
```

## Envoyer un courriel de `jdoe` à `bob`
```
curl -X POST "http://nomail.com/api/v1/courriels" --data '{"objet": "Important!", "contenu": Bob. Je écris pour vous dire bonjour.", "adresseExpéditeur":"amRvZUBlbWFpbC5jb20", "adresseDestinataire":"Ym9iQGVtYWlsLmNvbQ"}' -H "Content-Type: application/json" -H "Authorization: <TOKEN>"
```
#### Sortie:
```
{
    "_id": "607a7f55df6d4b6ad08abecb",
    "idCourrielReçu": "607a7f55df6d4b6ad08abecc",
    "objet": "Important!",
    "contenu": "Salutations Bob. Je vous écris pour vous dire bonjour.",
    "adresseExpéditeur": "amRvZUBlbWFpbC5jb20",
    "adresseDestinataire": "Ym9iQGVtYWlsLmNvbQ",
    "dateCréation": "2021-04-17T06:25:25.181Z"
}
```

## Obtenir un courriel envoyé de `jdoe` par id
#### Entrée:
```
curl "http://nomail.com/api/v1/courriels/envoye/607a7f55df6d4b6ad08abecb" -H "Content-Type: application/json" -H "Authorization: <TOKEN>"
```
#### Sortie:
```
{
    "_id": "607a7f55df6d4b6ad08abecb",
    "idCourrielReçu": "607a7f55df6d4b6ad08abecc",
    "objet": "Important!",
    "contenu": "Salutations Bob. Je vous écris pour vous dire bonjour.",
    "adresseExpéditeur": "amRvZUBlbWFpbC5jb20",
    "adresseDestinataire": "Ym9iQGVtYWlsLmNvbQ",
    "lu": false,
    "dateCréation": "2021-04-17T06:25:25.181Z"
}
```

## Obtenir une liste de courriels envoyés par `jdoe` avec son adresse courriel
#### Entrée:
```
curl "http://nomail.com/api/v1/courriels/envoye/amRvZUBlbWFpbC5jb20" -H "Content-Type: application/json" -H "Authorization: <TOKEN>"
```
#### Sortie:
```
[
    {
        "_id": "607a7f55df6d4b6ad08abecb",
        "lu": false,
        "objet": "Important!",
        "adresseExpéditeur": "amRvZUBlbWFpbC5jb20",
        "adresseDestinataire": "Ym9iQGVtYWlsLmNvbQ",
        "dateCréation": "2021-04-17T06:25:25.181Z"
    },
    {
        "_id": "607a8613e8cb1a6dbcb72862",
        "lu": false,
        "objet": "Encore plus important!",
        "adresseExpéditeur": "amRvZUBlbWFpbC5jb20",
        "adresseDestinataire": "Ym9iQGVtYWlsLmNvbQ",
        "dateCréation": "2021-04-17T06:54:12.005Z"
    }
]
```
